<div class="blog-sidebar col-4">
  <section>
    <?php
      $exclude = get_queried_object_id();
      $postcat = get_the_category( $exclude )[0];
      $posts = get_posts(array(
        'category' => $postcat->cat_ID
      ));
      
      if (sizeof($posts) > 1) {?>
      <header>
        <h5>Related Articles</h5>
      </header>
      <?php
        foreach ($posts as $side_post):
          the_post();
          $real_post = get_post($side_post);
          if ($real_post->ID == $exclude) {
            continue;
          }
          
          if ( has_post_thumbnail() ) { ?>
            <div class="related-article">
              <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                <div class="image-container">
                  <?php the_post_thumbnail(); ?>
                </div>
                <?php the_title(); ?>
              </a>
            </div>
          <?php }
        endforeach;
      }?>
  </section>
  <div class="sidebar-module sidebar-module-inset">
    <h4>About</h4>
    <p><?php the_author_meta( 'description' ); ?></p>
  </div>
  <div class="sidebar-module">
    <h4>Archives</h4>
    <ol class="list-unstyled">
      <?php wp_get_archives( 'type=monthly' ); ?>
    </ol>
  </div>
  <div class="sidebar-module">
    <h4>Elsewhere</h4>
    <ol class="list-unstyled">
      <li><a href="#">GitHub</a></li>
      <li><a href="#">Twitter</a></li>
      <li><a href="#">Facebook</a></li>
    </ol>
  </div>
</div>
<!-- /.blog-sidebar -->
