<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
   <?php if ( has_post_thumbnail() ) { ?>
      <div class="featured-image">
         <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_post_thumbnail(); ?></a>
      </div>
   <?php } ?>
	<div class="article-content">
		<?php if( get_post_format() ) { get_template_part( 'inc/post-formats' ); } ?>
		<header class="entry-header">
			<h3 class="entry-title">
				<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute();?>"><?php the_title(); ?></a>
			</h3>
		</header>
		<div class="entry-content excerpt">
			<?php
				the_excerpt();
			?>
			<a class="more-link" title="<?php the_title(); ?>" href="<?php the_permalink(); ?>"><span><?php _e( 'Read more'); ?></span></a>
		</div>
	</div>
</article>
<!-- /.blog-post -->
