<?php

function wpb_custom_new_menu() {
  register_nav_menus(
    array(
      'top-menu' => __( 'Top Menu' ),
    )
  );
}

function add_menu_link_class( $atts, $item, $args ) {
  if (property_exists($args, 'link_class')) {
    $atts['class'] = $args->link_class;
  }
  return $atts;
}
add_filter( 'nav_menu_link_attributes', 'add_menu_link_class', 1, 3 );

add_action( 'init', 'wpb_custom_new_menu' );
add_theme_support( 'post-thumbnails' ); 

function catCustom() {
  $cats = get_the_category($post->ID);
   $parent = get_category($cats[1]->category_parent);
   if (is_wp_error($parent)){
       $cat = get_category($cats[0]);
     }
     else{
       $cat = $parent;
     }
   echo '<a href="'.get_category_link($cat).'">'.$cat->name.'</a>';    
}
?>
