<article id="post-<?php the_ID(); ?>" <?php post_class('single-article'); ?>>
		<div class="top-info">
			<div class="category">
				<?php the_category(', '); ?>
			</div>
			<div class="posted">
				<?php the_date(); ?>
			</div>
		</div>
		<header class="entry-header">
			<h3 class="entry-title">
				<?php the_title(); ?>
			</h3>
		</header>
   <?php if ( has_post_thumbnail() ) { ?>
      <div class="featured-image">
         <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_post_thumbnail(); ?></a>
      </div>
   <?php } ?>
	<div class="article-content">
		<?php if( get_post_format() ) { get_template_part( 'inc/post-formats' ); } ?>
		<div class="entry-content">
			<?php
				the_content();
			?>
		</div>
	</div>
</article>
<!-- /.blog-post -->
