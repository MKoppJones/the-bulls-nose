<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>"/>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>The Bulls Nose - Here Lies The Truth, News You Can Trust</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
  <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
  <link rel="stylesheet" id="colormag_google_fonts-css" href="//fonts.googleapis.com/css?family=Open+Sans%3A400%2C600&amp;ver=5.4.1" type="text/css" media="all">
  <link href="<?php echo get_bloginfo( 'template_directory' );?>/blog.css" rel="stylesheet">
  <link href="<?php echo get_bloginfo( 'template_directory' );?>/stylesheets/main.css" rel="stylesheet">
  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  <?php wp_head();?>
</head>
<body <?php body_class(); ?>>
  <?php
  if ( function_exists( 'wp_body_open' ) ) {
    wp_body_open();
  }
  ?>
  <nav class="navbar navbar-expand-lg">
    <a class="navbar-brand" href="<?php echo get_bloginfo( 'wpurl' );?>">Logo</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
      <?php
        wp_nav_menu( array( 
          'theme_location' => 'top-menu',
          'container' => '',
          'menu_class' => 'navbar-nav',
          'list_item_class'  => 'nav-item',
          'link_class'   => 'nav-link m-2 menu-item'
        ) ); 
        ?>
    </div>
  </nav>
  <!-- <div class="blog-masthead">
    <div class="container">
      <nav class="blog-nav">
        <?php
          wp_nav_menu( array( 
            'theme_location' => 'top-menu', 
            'container_class' => 'custom-menu-class' ) ); 
          ?>
        <a class="blog-nav-item" href="
          <?php
            $args = array('posts_per_page' => 1, 'orderby' => 'rand');
            $rand_posts = get_posts($args);
            foreach ($rand_posts as $post) :
              the_permalink();
           endforeach; ?>"
          >
          Rand
        </a>
      </nav>
    </div>
  </div> -->

  <div class="container-fluid">
