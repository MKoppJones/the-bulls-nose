<?php get_header('single'); ?>
	<div class="row">
		<div class="blog-main col-8">
			<?php
				if ( have_posts() ):
					while ( have_posts() ) : the_post();
						get_template_part( 'content-single', get_post_format() );
					endwhile;
				endif;
			?>
		</div> <!-- /.blog-main -->
		<?php get_sidebar(); ?>
	</div>
<?php get_footer(); ?>
